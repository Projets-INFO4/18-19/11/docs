﻿# e-Tree

## 21.01.2019 - 27.01.2019
### Done
- Prise de contact avec la responsable du projet.
- Renseignement sur la thématique et le plan général du projet.
### To Do
- S'informer sur les différents aspects de fonctionnement de l'arbre, tels que les données mesurés et la manière dont ils sont stockés et transmis.
- Prendre en main les technos principales utilisés lors du projet (Ionic, JHipster, etc.)
- Participer à la réunion sur le projet le lundi 28 Janvier au FabLab et prendre contact avec les autres étudiants collaborateurs.

## 28.01.2019 - 3.02.2019
### Done
- Nous avons participé à la réunion de Lundi 28 Janvier avec la responsable du projet et les acteurs d'autres filières tels que l'IUT, Phelma et école d'architecture. Cela nous a permis de prendre contact avec les autres participants et de suivre l'évolution du projet qui est encore dans sa phase initiale. En effet la discussion porte sur des aspect comme l'endroit géographique d'emplacement de l'arbre, sa forme et mise en ouvre, son rendement énergique etc. 
- Nous avons suivi le cours de présentation des technologies Campus-IOT, NodeRed et JHipster organisé par Mr. Donsez. Nous avons appris les concepts de base de ces technos ce qui nous a donné une meilleure visualisation de la démarche à suivre pour ce projet.

### To Do  
- Réfléchir sur la structure et les spécifications de l'application, en utilisant par exemple une diagramme UML ou d'autres outils.
- Créer un repo JHipster gitlab et réussir à avoir un projet synchronisé entre les trois membres du groupe.
- Faire une simulation de récupération de données à travers Campus-IOT avec des fichiers générés par nous même. 

## 04.02.2019 - 10.02.2019
### Done
- Prise de contact avec le groupe IESE "Mesure de la qualité de l'air" et rendez-vous fixé à la semaine suivante
- Réflexions sur la structure de notre application et début de diagramme UML
- Découverte des différentes technologies (LoRa, NodeRED, JHipster)
### To Do
- S'informer sur les différents aspects de fonctionnement de l'arbre, tels que les données mesurés et la manière dont ils sont stockés et transmis.
- Continuer à réfléchir sur la structure et les spécifications de l'application, en utilisant par exemple une diagramme UML ou d'autres outils.
- Créer un repo JHipster gitlab et réussir à avoir un projet synchronisé entre les trois membres du groupe.
- Faire une simulation de récupération de données à travers Campus-IOT avec des fichiers générés par nous même.  

## 11.02.2019 - 17.02.2019
### Done
- Rendez-vous avec le groupe IESE "Mesure de la qualité de l'air"
- Plus d'informations sur les différentes mesures de la qualité de l'air
### To Do
- S'informer sur les différentes mesures relatives à la batterie et aux prises de l'arbre
- Continuer à réfléchir sur la structure et les spécifications de l'application, en utilisant par exemple une diagramme UML ou d'autres outils.
- Créer un repo JHipster gitlab et réussir à avoir un projet synchronisé entre les trois membres du groupe.
- Faire une simulation de récupération de données à travers Campus-IOT avec des fichiers générés par nous même.  

## 18.02.2019 - 24.02.2019
### Done
- Prise en main de NodeRED et Mosquitto (avec envoie de données locaux depuis Mosquitto vers NodeRED)
- Prise en main du module Ionic de JHipster
- Demande de contact avec les étudiants PHELMA pour obtenir plus d'informations sur les batteries et prises
- Créer un repo JHipster gitlab et réussir à avoir un projet synchronisé entre les trois membres du groupe.
### To Do
- S'informer sur les différentes mesures relatives à la batterie et aux prises de l'arbre
- Continuer à réfléchir sur la structure et les spécifications de l'application, en utilisant par exemple une diagramme UML ou d'autres outils.
- Faire une simulation de récupération de données à travers Campus-IOT avec des fichiers générés par nous même. 

## 04.03.2019 - 10.03.2019
### Done
- Via Node RED, émulation basique d'un capteur, avec envoie et réception d'un objet message
- Découverte d'un dashboard interressant pour notre projet
- Prise de décision d'utiliser Swagger avec le dashboard précédent plutôt que JHipster (à confirmer)
- Nouvelles informations récupérés sur la station météologique/qualité de l'air
### To Do
- S'informer sur les différentes mesures relatives à la batterie et aux prises de l'arbre
- Continuer à réfléchir sur la structure et les spécifications de l'application, en utilisant par exemple une diagramme UML ou d'autres outils.
- Faire une simulation de récupération de données à travers Campus-IOT avec des fichiers générés par nous même. 

## 11.03.2019 - 17.03.2019
### Done
- Prise en main du nouveau dashboard
- Prise en main de Swagger
- Simulation des capteurs météologiques de l'arbre
- Retour sur les mesures de la batterie : données transmises pas encore travaillées
### To Do
- Continuer à réfléchir sur la structure et les spécifications de l'application, en utilisant par exemple une diagramme UML ou d'autres outils.
- Faire une simulation de récupération de données à travers Campus-IOT avec des fichiers générés par nous même. 
- Définir et mettre en place la base de données
- Créer un conteneur docker de Node-RED

## 18.03.2019 - 24.03.2019
### Done
- Choix d'une base de données PostGres
- Simulation complète des données météologiques
- Reprise de contacts avec les équipes IESE et PHELMA (récupération des protocoles de l'équipe IESE)
- Avancement sur le dashboard et l'utilisation de Swagger
### To Do
- Vérifier que la simulation correspont aux protocoles établis
- Ajouter les paramètres en rapport avec la batterie à la simulation
- Finir de créer le dashboard
- Continuer à réfléchir sur la structure et les spécifications de l'application, en utilisant par exemple une diagramme UML ou d'autres outils.
- Faire une simulation de récupération de données à travers Campus-IOT avec des fichiers générés par nous même. 
- Mettre en place la base de données
- Créer un conteneur docker de Node-RED

## 25.03.2019 - 31.03.2019
### Done
- Implémenation des protocles de l'équipe qualité de l'air
- Prise en main de PostGres
- Définition des tables de la base de données (selon les données de qualité de l'air pour le moment)
### To Do
- Ajouter les paramètres en rapport avec la batterie à la simulation
- Finir de créer le dashboard
- Continuer à réfléchir sur la structure et les spécifications de l'application, en utilisant par exemple une diagramme UML ou d'autres outils.
- Faire une simulation de récupération de données à travers Campus-IOT avec des fichiers générés par nous même. 
- Mettre en place la base de données
- Créer un conteneur docker de Node-RED